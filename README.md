# Haumaru

### "Haumaru" is Maori for "security."

# What is Haumaru?

_Haumaru is still in its conceptual phase._

Haumaru is a library designed to address Internet-of-Things security vulnerabilities by providing a very light, memory-safe, multi-threadable singleton object which registers system inputs and outputs in a DMZ, and remains context aware.

More specifically, Haumaru treats every request and response (or every parameter and every result) as unvalidated and unverified until proven otherwise, and attempts to assign varying levels of trustworthiness and validity to all desired I/O operations. Haumaru will detect attempts to DoS its service and also provide recommendations. 

"Recommendations" is key to Haumaru's design: it is non-blocking, and leaves it up to the system administrator or developer to decide what to do with recommendations.

Haumaru is designed for all IoT, but with a specific focus on automated and unmanned systems communications, and remote sensors.

Haumaru is designed by people who love well made tools: tools which help and get out of the way rather than hinder development.

# How will I use Haumaru?

The plan for Haumaru is it will come in two forms:

 1. A daemon – to install (and configure) on devices
 2. A library – to include in IoT development projects

# Haumaru is written in Rust

Haumaru is written in Rust to take advantage of modern code writing paradigms while also being highly performant. There are planned branches in C and C++. 

More importantly, Rust binaries can be called __by C and C++ applications__ with little more effort than including Python or C libary (if you're working with C++).

Perhaps most importantly, Rust packages **all** its dependencies, so if a library is included or a daemon is installed, it will never have a missing dependency.

# Where do I submit an issue, or suggest a feature?

You can open up issues or suggest features here.

# Can I contribute?

Yes! We love contributors, and encourage all who are interested to submit an issue.

# Planned features

 - Consumer & developer friendly freemium pricing model
 - Machine Learning for context and operations
    - Such as, "is this a drone?" and "am I being commanded to crash by a non-trusted party?"
